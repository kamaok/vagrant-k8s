

Поднятие 3-х master-нод и 2-х worker-нод с автоматическим provisioning:
 - всех необходимых настроек для подготовки нод к установке k8s-кластера
 - установка Haproxy на master0{1..2}-нодах в качестве балансировщика нагрузки на транспортном уровне (level 4)

   а) балансировка входящих kube-api-запросов, поступающих от kubelet и других клиентов на все kube-api поды запущенные на master-нодах

   б) балансировка входящих клиентских/пользовательских запросов на порт 80 на ingres-nginx поды, запущенные на worker-нодах
 - установка keepalived на master0{1..2}-нодах в качестве реализации Linux Virtual Server(LVS) для работы с виртуальным/плавающим IP-адресом для kube-api(порт 8443) и пользовательских-запроcов(порт 80)

Ручная установка k8s-кластера с помощью kubeadm на подготовленных Vagrant-ом виртуальных машинах

Структурная схема имеет вид:

![vagrnat-k8s](architecture-schema.png)

### Подготовка нод кластера

Активация Vagrant-файла для нужной версии Centos (7/8)

В данном случае для Centos 8
```bash
ln -sf Vagrantfile-cluster-centos-8 Vagrantfile
```

Проверка синтаксиса конфигурационого файла Vagrant
```bash
vagrant validate
```

Создание и автоматическая настройка всех нод кластера
```bash
vagrant up
```

Установка keepalived на `master01`
```bash
sed -i 's|#node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master01.sh|node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master01.sh|' Vagrantfile
```
```bash
vagrant provision master01 --provision-with setup-keepalived
```
Установка keepalived на master02
```bash
sed -i 's|node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master01.sh|#node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master01.sh|' Vagrantfile
```
```bash
sed -i 's|#node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master02.sh|node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master02.sh|' Vagrantfile
```
```bash
vagrant provision master02 --provision-with setup-keepalived
```
Отключим активированный предыдущей командой provisioning keepalived
```bash
sed -i 's|node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master02.sh|#node.vm.provision "setup-keepalived", type: "shell", :path => "centos/setup-keepalived-master02.sh|' Vagrantfile
```
Проверка отработки всех provisioning-задач/скриптов:

На master-ах(например, на master01)
```bash
vagrant ssh master01
```
```bash
sudo su -l
```
```bash
iptables -S
free -mw
getenforce
cat /etc/hosts
systemctl status containerd
cat /etc/sysctl.d/k8s.conf
lsmod | grep -E 'overlay|br_netfilter'
kubelet --version
kubeadm version
kubectl version --client
cat /etc/sysctl.d/k8s.conf
systemctl status haproxy
systemctl status keepalived
netstat -nlptu | grep -E '8443|80|61000'
ip a sh | grep 192.168.66
```

На node-ах (например, на node01)

```bash
vagrant ssh node01
```
```bash
sudo su -l
```
```bash
iptables -S
free -mw
getenforce
cat /etc/hosts
systemctl status containerd
cat /etc/sysctl.d/k8s.conf
lsmod | grep -E 'overlay|br_netfilter'
kubelet --version
kubeadm version
kubectl version --client
cat /etc/sysctl.d/k8s.conf
```

### Установка Kubernetes кластера
Инициализация кластера

```bash
vagrant ssh master01
```
```bash
kubeadm init --apiserver-advertise-address "192.168.66.11" --control-plane-endpoint "loadbalancer:8443" --kubernetes-version "1.21.2" --upload-certs --pod-network-cidr "10.200.0.0/16"
```

```bash
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of the control-plane node running the following command on each as root:

  kubeadm join loadbalancer:8443 --token kgo0n6.7b1hqnbk07i20vij \
	--discovery-token-ca-cert-hash sha256:75be71dd534d9a29b30bb61bc642160048aee98b04f804866ed308695f821498 \
	--control-plane --certificate-key 6b799c25419f077da2b23b02cf733fa67444986f5d2dfdb2ad1a785171f0fefb

Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use
"kubeadm init phase upload-certs --upload-certs" to reload certs afterward.

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join loadbalancer:8443 --token kgo0n6.7b1hqnbk07i20vij \
	--discovery-token-ca-cert-hash sha256:75be71dd534d9a29b30bb61bc642160048aee98b04f804866ed308695f821498
```

Настройка файла для аутентификации и авторизации в k8s для master01-ноды
```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

```bash
# kubectl get pod -A
NAMESPACE     NAME                               READY   STATUS    RESTARTS   AGE
kube-system   coredns-558bd4d5db-dhj94           0/1     Pending   0          2m14s
kube-system   coredns-558bd4d5db-dvcrr           0/1     Pending   0          2m14s
kube-system   etcd-master01                      1/1     Running   0          2m14s
kube-system   kube-apiserver-master01            1/1     Running   0          2m27s
kube-system   kube-controller-manager-master01   1/1     Running   0          2m14s
kube-system   kube-proxy-bxj4q                   1/1     Running   0          2m14s
kube-system   kube-scheduler-master01            1/1     Running   0          2m14s
```

Установка calico в качестве Container Network Interface
C официального источника:
```bash
curl -o /home/vagrant/calico.yaml https://docs.projectcalico.org/manifests/calico.yaml
```
```bash
kubectl apply -f /home/vagrant/calico.yaml
```
```bash
kubectl get pod -n kube-system
```
```bash
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-78d6f96c7b-sctz9   1/1     Running   0          3m45s
calico-node-kbfwb                          1/1     Running   0          3m45s
coredns-558bd4d5db-dhj94                   1/1     Running   0          8m34s
coredns-558bd4d5db-dvcrr                   1/1     Running   0          8m34s
etcd-master01                              1/1     Running   0          8m34s
kube-apiserver-master01                    1/1     Running   0          8m47s
kube-controller-manager-master01           1/1     Running   0          8m34s
kube-proxy-bxj4q                           1/1     Running   0          8m34s
kube-scheduler-master01                    1/1     Running   0          8m34s
```

И только после того, как поднялись контейнеры с `coredns` и `calico` добавляем остальные мастера  в кластер

Дополнительно к команде по добавлению ноды в кластер, я принудительно добавил параметр `--apiserver-advertise-address`
со значением IP-адреса ноды. через который должно происходить взаимодействие с остальными членами кластера

в нашем случае `192.168.66.12`

Добавление второго мастера в k8s-кластер

```bash
vagrant ssh master02
```
```bash
kubeadm join loadbalancer:8443 --token kgo0n6.7b1hqnbk07i20vij \
--discovery-token-ca-cert-hash sha256:75be71dd534d9a29b30bb61bc642160048aee98b04f804866ed308695f821498 \
--control-plane --certificate-key 6b799c25419f077da2b23b02cf733fa67444986f5d2dfdb2ad1a785171f0fefb \
--apiserver-advertise-address "192.168.66.12"
```
Настройка файла для аутентификации и авторизации в k8s для master02-ноды
```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Добавление третьего мастера в k8s-кластер
```bash
vagrant ssh master03
```
```bash
kubeadm join loadbalancer:8443 --token kgo0n6.7b1hqnbk07i20vij \
--discovery-token-ca-cert-hash sha256:75be71dd534d9a29b30bb61bc642160048aee98b04f804866ed308695f821498 \
--control-plane --certificate-key 6b799c25419f077da2b23b02cf733fa67444986f5d2dfdb2ad1a785171f0fefb \
--apiserver-advertise-address "192.168.66.13"
```

Настройка файла для аутентификации и авторизации в k8s для master03-ноды
```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Добавление первой рабочей ноды в k8s-кластер

```bash
vagrant ssh node01
```
```bash
kubeadm join loadbalancer:8443 --token kgo0n6.7b1hqnbk07i20vij \
--discovery-token-ca-cert-hash sha256:75be71dd534d9a29b30bb61bc642160048aee98b04f804866ed308695f821498
```

Добавление второй рабочей ноды в k8s-кластер
```bash
vagrant ssh node02
kubeadm join loadbalancer:8443 --token kgo0n6.7b1hqnbk07i20vij \
--discovery-token-ca-cert-hash sha256:75be71dd534d9a29b30bb61bc642160048aee98b04f804866ed308695f821498
```
```bash
kubectl get node
NAME       STATUS   ROLES                  AGE     VERSION
master01   Ready    control-plane,master   17m     v1.21.2
master02   Ready    control-plane,master   7m26s   v1.21.2
master03   Ready    control-plane,master   4m1s    v1.21.2
node01     Ready    <none>                 114s    v1.21.2
node02     Ready    <none>                 56s     v1.21.2
```

```bash
kubectl get pod -A
```
```bash
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-78d6f96c7b-8zhm9   1/1     Running   0          15m
kube-system   calico-node-7wrx5                          1/1     Running   0          5m28s
kube-system   calico-node-9ppk4                          1/1     Running   0          6m25s
kube-system   calico-node-p5tpb                          1/1     Running   0          11m
kube-system   calico-node-t8n6q                          1/1     Running   0          8m33s
kube-system   calico-node-zmb9x                          1/1     Running   0          15m
kube-system   coredns-558bd4d5db-rx875                   1/1     Running   0          21m
kube-system   coredns-558bd4d5db-vc26d                   1/1     Running   0          21m
kube-system   etcd-master01                              1/1     Running   0          21m
kube-system   etcd-master02                              1/1     Running   0          11m
kube-system   etcd-master03                              1/1     Running   0          8m30s
kube-system   kube-apiserver-master01                    1/1     Running   0          21m
kube-system   kube-apiserver-master02                    1/1     Running   0          11m
kube-system   kube-apiserver-master03                    1/1     Running   0          8m31s
kube-system   kube-controller-manager-master01           1/1     Running   1          21m
kube-system   kube-controller-manager-master02           1/1     Running   0          11m
kube-system   kube-controller-manager-master03           1/1     Running   0          8m31s
kube-system   kube-proxy-24xkd                           1/1     Running   0          8m33s
kube-system   kube-proxy-5bgpt                           1/1     Running   0          6m25s
kube-system   kube-proxy-6ff2m                           1/1     Running   0          11m
kube-system   kube-proxy-7f9zc                           1/1     Running   0          5m28s
kube-system   kube-proxy-gfqk5                           1/1     Running   0          21m
kube-system   kube-scheduler-master01                    1/1     Running   1          21m
kube-system   kube-scheduler-master02                    1/1     Running   0          11m
kube-system   kube-scheduler-master03                    1/1     Running   0          8m31s
```

После запуска master-ов проверяем поднятие всех мастеровых компонентов k8s включая haproxy
```bash
netstat -nlptu | grep -E '6443|23[78][91]|8443|1025[0697]'
```
```bash
tcp        0      0 127.0.0.1:8443          0.0.0.0:*               LISTEN      4283/haproxy
tcp        0      0 192.168.66.100:8443     0.0.0.0:*               LISTEN      4283/haproxy
tcp        0      0 192.168.66.11:2379      0.0.0.0:*               LISTEN      30735/etcd
tcp        0      0 127.0.0.1:2379          0.0.0.0:*               LISTEN      30735/etcd
tcp        0      0 127.0.0.1:2381          0.0.0.0:*               LISTEN      30735/etcd
tcp        0      0 127.0.0.1:10257         0.0.0.0:*               LISTEN      4185/kube-controlle
tcp        0      0 127.0.0.1:10259         0.0.0.0:*               LISTEN      4177/kube-scheduler
tcp6       0      0 :::10250                :::*                    LISTEN      30836/kubelet
tcp6       0      0 :::6443                 :::*                    LISTEN      30706/kube-apiserve
tcp6       0      0 :::10256                :::*                    LISTEN      30990/kube-proxy
```

На рабочих нодах провереям поднятие kubelet и kube-proxy
```bash
netstat -nlptu | grep -E '1025[06]'
tcp6       0      0 :::10256                :::*                    LISTEN      2061/kube-proxy
tcp6       0      0 :::10250                :::*                    LISTEN      993/kubelet
```

Также требуется вручную настроить конфигурационный файл `crictl.yaml` для работы с подами/контейнерами/образами (взаимодействия с сri `containerd`)
```bash
cat <<EOF > /etc/crictl.yaml
runtime-endpoint: unix:///run/containerd/containerd.sock
image-endpoint: unix:///run/containerd/containerd.sock
timeout: 10
EOF
```
```bash
crictl pods
crictl ps
crictl images
```

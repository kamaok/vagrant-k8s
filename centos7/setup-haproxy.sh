#!/usr/bin/env bash

set -e
set -u

#VIP="192.168.66.100"
VIP=$1

sudo yum install centos-release-scl -y
sudo yum install rh-haproxy18-haproxy rh-haproxy18-haproxy-syspaths -y
sudo rm -f /etc/haproxy/haproxy.cfg || true

cat <<EOF | sudo tee /etc/rsyslog.d/haproxy.conf
local2.* /var/opt/rh/rh-haproxy18/log/haproxy.log
EOF

sudo systemctl restart rsyslog && systemctl status rsyslog

sudo ln -sf /opt/rh/rh-haproxy18/root/usr/sbin/haproxy /usr/sbin/haproxy

sudo ln -sf /etc/opt/rh/rh-haproxy18/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg

cat <<EOF | sudo tee /etc/sysctl.d/haproxy.conf
net.ipv4.ip_nonlocal_bind = 1
EOF

sudo sysctl --system

sysctl -a 2>/dev/null | grep net.ipv4.ip_nonlocal_bind

cat <<EOF | sudo tee /etc/haproxy/haproxy.cfg
global
    #log        127.0.0.1 local2
    log         /dev/log local2

    chroot      /var/opt/rh/rh-haproxy18/lib/haproxy
    pidfile     /var/run/rh-haproxy18-haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/opt/rh/rh-haproxy18/lib/haproxy/stats

    # utilize system-wide crypto-policies
    ssl-default-bind-ciphers PROFILE=SYSTEM
    ssl-default-server-ciphers PROFILE=SYSTEM

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------


defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    #option http-server-close
    #option forwardfor      except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 10000


frontend Statistics
        bind 0.0.0.0:61000
        mode http
        stats enable
        stats uri /haproxy
        stats realm Restricted\ Area
        stats auth admin:mypassword
        stats refresh 10s
        stats hide-version
        stats show-legends
        stats show-node


frontend k8s-api
  bind $VIP:8443
  bind 127.0.0.1:8443
  mode tcp
  timeout client 4h
  option tcplog
  default_backend k8s-api

frontend applications-frontend
        bind 0.0.0.0:80
        # timeout client  16800h
        mode http
        #mode tcp
        #option tcplog
        default_backend applications-backend


backend k8s-api
  mode tcp
  timeout server 4h
  option tcp-check
  balance roundrobin
  #default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100
  default-server inter 5s fastinter 3s fall 3 rise 2
  server master01 192.168.66.11:6443 check maxconn 1024
  server master02 192.168.66.12:6443 check maxconn 1024
  server master03 192.168.66.13:6443 check maxconn 1024


backend applications-backend
        mode http
        balance roundrobin
#	option httpclose
        option forwardfor
        option redispatch

        cookie SRVNAME insert
        default-server inter 3s fastinter 2s fall 3 rise 2
        server node01 192.168.66.21:80 cookie node01 check maxconn 50000 weight 100
        server node02 192.168.66.22:80 cookie node02 check maxconn 50000 weight 100
EOF

haproxy -c -f /etc/haproxy/haproxy.cfg

sudo systemctl enable haproxy --now

systemctl status haproxy

netstat -nlptu | grep -E '8443|80|61000'

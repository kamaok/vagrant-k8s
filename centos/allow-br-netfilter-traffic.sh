#!/usr/bin/env bash

set -e

# Load required modules
# overlay - for containerd as CRI (not require for docker as CRI)
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
overlay
EOF

sudo modprobe br_netfilter
sudo modprobe overlay

lsmod | grep -E 'overlay|br_netfilter'

# Allow iptables to see bridge-traffic and enable ip forwarding
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system
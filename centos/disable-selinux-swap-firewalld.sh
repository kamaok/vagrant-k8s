#!/usr/bin/env bash

set -e

# Move Selinux to pemissive mode
sudo setenforce 0 && getenforce
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
grep -w SELINUX /etc/selinux/config | grep -v "^#"

# Disable swap
sudo sed -i '/ swap / s/^/#/' /etc/fstab
grep swap /etc/fstab
sudo swapoff -a
free -mw | grep [S]wap

# Disable firewalld
sudo systemctl stop firewalld
sudo systemctl disable firewalld

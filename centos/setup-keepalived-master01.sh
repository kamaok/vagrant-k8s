#!/usr/bin/env bash

set -e

sudo yum install postfix -y
sudo sed -i 's/inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
sudo postfix check
sudo systemctl restart postfix; sudo systemctl enable postfix
netstat -nlptu  | grep 25

sudo yum install keepalived psmisc -y
[ -f  /etc/keepalived/keepalived.conf.original ] || sudo cp /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.conf.original

cat <<EOF | sudo tee /etc/keepalived/keepalived.conf
global_defs {
notification_email {
user@domain.com
}

notification_email_from keepalived@master01
smtp_server localhost
smtp_connect_timeout 30
router_id master01
script_user root root
enable_script_security
}

vrrp_script chk_haproxy {
script "/usr/bin/killall -0 haproxy"   # verify the pid existance
interval 2                    # check every 2 seconds
fall 2                        # If script returns non-zero 2 times in succession, enter FAULT state
rise 3                        # If script returns zero 3 times in succession, exit FAULT state
#weight 2                      # add 2 points of prio if OK
weight -60                      # remove  60 points of prio if NOT OK
}

vrrp_instance VI_1 {
interface eth1 # interface to monitor
state MASTER
virtual_router_id 130 # Assign one ID for this route
priority 100 # 100 on master, 50 on backup

#nopreempt

#track_interface {
#  eth1
#}

authentication {
auth_type PASS
auth_pass 12345678
}

 unicast_src_ip 192.168.66.11
 unicast_peer {
        192.168.66.12
    }

#smtp_alert

advert_int 1

virtual_ipaddress {
#192.168.66.100 # the virtual IP
192.168.66.100/24 brd 192.168.66.255 label eth1:1
}

track_script {
chk_haproxy
  }
}
EOF

systemctl enable keepalived --now
systemctl status keepalived

ip a sh  | grep 192.168.66.100 || true
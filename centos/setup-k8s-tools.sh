#!/usr/bin/env bash

set -e
set -u

#K8S_VERSION="1.21.2"
K8S_VERSION=$1

cat <<'EOF' | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

sudo yum install -y kubelet-${K8S_VERSION} kubeadm-${K8S_VERSION} kubectl-${K8S_VERSION} --disableexcludes=kubernetes

sudo systemctl enable --now kubelet


# Настройка kubectl автодополнения
sudo yum install bash-completion -y
cat <<EOF >> ~/.bashrc

source /etc/profile.d/bash_completion.sh
source <(kubectl completion bash)
EOF

kubelet --version
kubeadm version
kubectl version --client

#!/usr/bin/env bash

set -e
set -u

#IFNAME=$1
#DOMAINNAME="local"
DOMAINNAME="$1"

#ADDRESS="$(ip -4 addr show $IFNAME | grep "inet" | head -1 |awk '{print $2}' | cut -d/ -f1)"
#sed -i -e "s/^.*${HOSTNAME}.*/${ADDRESS} ${HOSTNAME} ${HOSTNAME}.${DOMAINNAME}/" /etc/hosts

# Delete default record something like it `127.0.0.1 centos7.localdomain`
sudo sed -i '/centos[78]/d' /etc/hosts
# Delete default record something like  `127.0.1.1 $HOSTNAME $HOSTNAME`
sudo sed -i "/$HOSTNAME/d" /etc/hosts

cat << EOF | sudo tee -a /etc/hosts > /dev/null

192.168.66.11 master01 master01.$DOMAINNAME
192.168.66.12 master02 master02.$DOMAINNAME
192.168.66.13 master03 master03.$DOMAINNAME
192.168.66.21 node01 node01.$DOMAINNAME
192.168.66.22 node02 node02.$DOMAINNAME
192.168.66.100 loadbalancer loadbalancer.$DOMAINNAME
EOF

cat /etc/hosts

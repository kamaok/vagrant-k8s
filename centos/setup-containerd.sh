#!/usr/bin/env bash

set -e
set -u

#CONTAINERD_VERSION=1.4.6
CONTAINERD_VERSION=$1

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y containerd.io-${CONTAINERD_VERSION}

rpm -qa | grep containerd

sudo mkdir -p /etc/containerd && containerd config default | sudo tee /etc/containerd/config.toml > /dev/null
sudo sed -i '/plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options/ a\            SystemdCgroup = true' /etc/containerd/config.toml

sudo systemctl enable containerd --now
systemctl status containerd

ps aux | grep [c]ontainerd
